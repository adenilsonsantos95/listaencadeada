package Teste;

import ListaEncadeada.Codigo.ListaEncadeada;
import ListaEncadeada.Codigo.ListaException;

import java.util.Arrays;

public class TesteLista {
    public static void main(String[] args) {
        ListaEncadeada numeros = new ListaEncadeada();
        try {
            numeros.adicionar(5); numeros.adicionar(6);
            numeros.adicionar(8); numeros.adicionar(34);
            numeros.adicionar(23);
            System.out.println(numeros.obter(23));
            System.out.println(numeros.obter(78));
            System.out.println(numeros.obter(5));
            System.out.println(numeros.toString());
        } catch (ListaException e) {
            e.printStackTrace();
        }

    }
}
