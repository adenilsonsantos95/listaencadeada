package Teste;

import ListaEncadeada.Codigo.ListaException;
import PilhaEncadeada.Codigo.Pilha;

public class TestePilha {
    public static void main(String[] args) {
        Pilha numeros = new Pilha();
        try {
            numeros.empillhar(48); numeros.empillhar(90); numeros.empillhar(65);
            numeros.empillhar(1); numeros.empillhar(32); numeros.empillhar(2);
            System.out.println(numeros.obter(90));
            System.out.println(numeros.toString());
            numeros.desempilhar();
            System.out.println(numeros.toString());
        } catch (ListaException e) {
            e.printStackTrace();
        }
    }
}
