package PilhaEncadeada.Codigo;

import ListaEncadeada.Codigo.ListaException;

public class Pilha implements IPilha {
    private No primeiro;
    private No ultimo;
    int quantidade;

    public Pilha() {
        this.primeiro = null;
        this.ultimo = null;
        this.quantidade = 0;
    }

    @Override
    public void empillhar(Object o) throws ListaException {
        No no = new No(o);
        if(this.getQtd() == 0) {
            this.primeiro = no;
            this.ultimo = this.primeiro;
        }else {
            this.ultimo.setProximo(no);
            this.ultimo = no;
        }
        this.quantidade++;
    }

    @Override
    public void desempilhar() throws ListaException {
        if (this.getQtd() == 0){
            throw new ListaException("Pilha vazia.");
        }else {
            No aux;
            for (aux = this.primeiro; aux.getProximo() != this.ultimo; aux = aux.getProximo()){}
            aux.setProximo(null);
            this.ultimo = aux;
        }
    }

    @Override
    public Object getTopo() throws ListaException {
        if (this.getQtd() == 0){
            throw new ListaException("Pilha vazia!");
        }
        return this.ultimo.getElemento();
    }

    public Object obter(Object chave){
        if(this.getQtd() == 0) {
            return null;
        }else if(this.primeiro.getElemento().equals(chave)) {
            return this.primeiro.getElemento();
        }else if(this.ultimo.getElemento().equals(chave)) {
            return this.ultimo.getElemento();
        }else {
            No aux = this.primeiro;
            for (int i = 0; i < this.getQtd(); i++) {
                if (aux.getElemento().equals(chave)) {
                    return (Object) aux.getElemento();
                }
                aux = aux.getProximo();
            }
            return null;
        }
    }

    @Override
    public int getQtd() {
        return this.quantidade;
    }

    @Override
    public String toString() {
        String pilha = "Pilha =";
        for (No aux = this.primeiro; aux != null; aux = aux.getProximo()){
            pilha = pilha + " " + aux.getElemento();
        }
        return pilha;
    }
}
