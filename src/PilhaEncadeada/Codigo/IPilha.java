package PilhaEncadeada.Codigo;

import ListaEncadeada.Codigo.ListaException;

public interface IPilha {
    public void empillhar(Object o ) throws ListaException;
    public void desempilhar() throws ListaException;
    public Object getTopo() throws ListaException;
    public int getQtd();
}
