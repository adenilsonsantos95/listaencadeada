package FilaEncadeada.Codigo;

import ListaEncadeada.Codigo.ListaException;
import ListaEncadeada.Codigo.No;

public class Fila<T> implements IFila<T> {
    private No primeiro;
    private No ultimo;
    private int quantidade;

    public Fila(){
        this.primeiro = null;
        this.ultimo = null;
        this.quantidade = 0;
    }

    @Override
    public void enfileirar(T elemento) throws ListaException {
        No no = new No(elemento);
        if (this.getQtd() == 0){
            this.primeiro = no;
            this.ultimo = no;
        }else{
            this.ultimo.setProximo(no);
            this.ultimo = no;
        }
        this.quantidade++;
    }

    @Override
    public boolean estaVazia() {
        return this.getQtd() == 0;
    }

    @Override
    public T espiar() throws ListaException {
        return (T)this.primeiro.getElemento();
    }

    @Override
    public T desenfileirar() throws ListaException {
        if (this.getQtd() == 0) {
            throw new ListaException("A fila está vazia!!!");
        }
        No aux = this.primeiro;
        this.primeiro = this.primeiro.getProximo();
        return (T)aux.getElemento();
    }

    @Override
    public int getQtd() {
        return 0;
    }

    @Override
    public String toString() {
        String fila = "Fila =";
        for (No aux = this.primeiro; aux != null; aux = aux.getProximo()){
            fila = fila + " " + aux.getElemento();
        }
        return fila;
    }
}
