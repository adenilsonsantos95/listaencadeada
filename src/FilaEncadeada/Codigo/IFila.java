package FilaEncadeada.Codigo;

import ListaEncadeada.Codigo.ListaException;

public interface IFila<T> {
    public void enfileirar(T elemento) throws ListaException;
    public boolean estaVazia();
    public T espiar() throws ListaException;
    public T desenfileirar() throws ListaException;
    public int getQtd();
}
