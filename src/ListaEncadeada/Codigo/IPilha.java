package ListaEncadeada.Codigo;

public interface IPilha extends ILista<Object> {
    @Override
    void adicionar(Object elemento) throws ListaException;

    @Override
    void remover(Object chave) throws ListaException;

    @Override
    boolean contem(Object chave);

    @Override
    Object obter(Object chave);

    @Override
    int getQtd();
}
