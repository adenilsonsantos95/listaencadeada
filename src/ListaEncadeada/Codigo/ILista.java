package ListaEncadeada.Codigo;

public interface ILista<T> {
    public void adicionar( T elemento) throws ListaException;
    public void remover( T chave) throws ListaException;
    public boolean contem(T chave);
    public T obter(T chave);
    public int getQtd();
}
