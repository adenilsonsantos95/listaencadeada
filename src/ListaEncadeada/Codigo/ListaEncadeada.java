package ListaEncadeada.Codigo;

public class ListaEncadeada<T extends Comparable<T>> implements ILista<T> {

    private No primeiro;
    private No ultimo;
    private int quantidade;

    public ListaEncadeada() {
        this.primeiro = null;
        this.ultimo = null;
        this.quantidade = 0;
    }

    @Override
    public void adicionar(T elemento) throws ListaException {
        if(this.getQtd() == 0) {
            this.adcionarNoInicio(elemento);
        }else {
            No no = new No(elemento);
            this.ultimo.setProximo(no);
            this.ultimo = no;
            this.quantidade++;
        }
    }

    public void adcionarNoInicio(T elemento) {
        No no = new No(elemento);
        if(this.getQtd() == 0) {
            this.primeiro = no;
            this.ultimo = this.primeiro;
        }else {
            no.setProximo(this.primeiro);
            this.primeiro = no;
        }
        this.quantidade++;
    }

    // metodo pedido na questao 1 da prova

    public T obter(T chave) {
        if(this.getQtd() == 0) {
            return null;
        }else if(this.primeiro.getElemento().equals(chave)) {
            return (T) this.primeiro.getElemento();
        }else if(this.ultimo.getElemento().equals(chave)) {
            return (T) this.ultimo.getElemento();
        }else {
            No aux = this.primeiro;
            for (int i = 0; i < this.getQtd(); i++) {
                if(aux.getElemento().equals(chave)) {
                    return (T) aux.getElemento();
                }
                aux = aux.getProximo();
            }
        }
        return null;
    }
    // fim do metodo pedido na questao 1 da prova

    @Override
    public void remover(T chave) throws ListaException {
        No aux = this.primeiro;
        No temp = null;
        for (int i = 0; i < this.getQtd(); i++) {
            if(aux.getElemento().equals(chave)) {
                temp.setProximo(aux.getProximo());
                break;
            }
            temp = aux;
            aux = aux.getProximo();
        }
        this.quantidade--;
    }

    @Override
    public boolean contem(T chave) {
        if(this.getQtd() == 0) {
            return false;
        }else if(this.primeiro.getElemento().equals(chave)){
            return true;
        }else if(this.ultimo.getElemento().equals(chave)){
            return true;
        }else {
            No aux = this.primeiro;
            for (int i = 0; i < this.getQtd(); i++) {
                if(aux.getElemento().equals(chave)) {
                    return true;
                }
                aux = aux.getProximo();
            }
        }
        return false;
    }

    @Override
    public int getQtd() {
        return this.quantidade;
    }

    @Override
    public String toString() {
        String lista = "Lista =";
        for (No aux = this.primeiro; aux != null; aux = aux.getProximo()){
            lista = lista + " " + aux.getElemento();
        }
        return lista;
    }
}
